package rashon.com.bbcmvvm.viewmodel

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import rashon.com.bbcmvvm.model.BreakingBadCharacter

class DetailsViewModelFactory(private val breakingBadCharacter: BreakingBadCharacter, private val app: Application): ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(DetailsViewModel::class.java)){
            return DetailsViewModel(breakingBadCharacter, app) as T
        }
        throw IllegalArgumentException("Unknown ViewModel Class")
    }
}