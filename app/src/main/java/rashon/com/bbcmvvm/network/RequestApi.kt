package rashon.com.bbcmvvm.network

import rashon.com.bbcmvvm.model.BreakingBadCharacter
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

private const val URL = "https://breakingbadapi.com/api/"
private val retrofit = Retrofit.Builder()
    .addConverterFactory(GsonConverterFactory.create())
    .baseUrl(URL)
    .build()
interface RequestApi{
    @GET("characters")
    suspend fun getCharacters(): ArrayList<BreakingBadCharacter>
}
object BBCApi{
    val retrofitService: RequestApi by lazy {
        retrofit.create(RequestApi::class.java)
    }
}
//class RequestApi {
//    lateinit var appContext: Context
//    lateinit var result: JSONArray
//
//    fun setContext(cont: Context){
//        appContext = cont
//    }
//
//    init {
//        makeRequest()
//    }
//
//    private fun makeRequest(){
//        val queue: RequestQueue = Volley.newRequestQueue(appContext)
//        val url = "https://breakingbadapi.com/api/characters"
//        val stringRequest = JsonArrayRequest(
//                Request.Method.GET, url,null,
//                { response ->
//                    result = response
////                    fetchJson(view, response)
//                },
//                { error -> Log.i("RRRRR", "That didn't work! $error") })
//        queue?.add(stringRequest)
//    }
//}