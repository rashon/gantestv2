package rashon.com.bbcmvvm.utils


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import rashon.com.bbcmvvm.R
import rashon.com.bbcmvvm.model.BreakingBadCharacter

class AdapterBBC(listOfChars: ArrayList<BreakingBadCharacter>, private val listener: (BreakingBadCharacter) -> Unit):
    RecyclerView.Adapter<AdapterBBC.ItemViewHolder>() {

    var displayedChars = listOfChars
//        var filteredCharacters = ArrayList<BreakingBadCharacter>()
//    var dataset = listOfChars
//
//    init {
//        filteredCharacters = dataset
//    }

    class ItemViewHolder(private val view: View): RecyclerView.ViewHolder(view) {
        val charName = view.findViewById<TextView>(R.id.charName)
        val cardViewImage = view.findViewById<ImageView>(R.id.cardViewImage)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val adapterLayout = LayoutInflater.from(parent.context).inflate(R.layout.list_item, parent, false)
        return ItemViewHolder(adapterLayout)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {

        val item = displayedChars[position]
        holder.charName.text = item.name
        val picasso = Picasso.get()
        picasso.load(item.img).into(holder.cardViewImage)
        holder.itemView.setOnClickListener{
            listener(item)
        }
    }

    override fun getItemCount(): Int {
        return displayedChars.size
    }

//    override fun getFilter(): Filter {
//        return object : Filter() {
//            override fun performFiltering(constraint: CharSequence?): FilterResults {
//                val charSearch = constraint.toString()
//                if (charSearch.isEmpty()) {
//                    filteredCharacters = dataset
//                }
//                else if(charSearch.startsWith("87RrSx1FkL", ignoreCase = false)){
//                    var seasonsSelected: List<String> = mutableListOf<String>()
//                    var sToList = charSearch.substring(10)
//                    if (sToList.length > 2) {
//                        sToList = sToList.removePrefix("[")
//                        sToList = sToList.removeSuffix("]")
//                        seasonsSelected = sToList.split(", ")
//                        Log.d("RRRRRFilte", seasonsSelected.toString())
//                        val resultList = ArrayList<BreakingBadCharacter>()
//                        for (row in dataset) {
//                            for (season in seasonsSelected) {
//                                if (row.appearance.contains(season.toInt())) {
//                                    if (!resultList.contains(row)) {
//                                        resultList.add(row)
//                                    }
//                                }
//                            }
//
//                        }
//                        filteredCharacters = resultList
//                    }
//                    else{
//                        filteredCharacters = dataset
//                    }
//                }
//                else {
//                    val resultList = ArrayList<BreakingBadCharacter>()
//                    for (row in dataset) {
//                        if (row.name.toLowerCase(Locale.ROOT).contains(charSearch.toLowerCase(Locale.ROOT))){
//                            resultList.add(row)
//                        }
//                    }
//                    filteredCharacters = resultList
//                }
//                val filterResults = FilterResults()
//                filterResults.values = filteredCharacters
//                return filterResults
//            }
//
//            @Suppress("UNCHECKED_CAST")
//            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
//                filteredCharacters = results?.values as ArrayList<BreakingBadCharacter>
//                notifyDataSetChanged()
//            }
//        }
//    }
}


