package rashon.com.bbcmvvm.viewmodel;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 2}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0010\b\n\u0002\b\r\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\b\n\u0002\u0010\u000e\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u001a\u001a\u00020\u001bJ\u000e\u0010\u001c\u001a\u00020\u001b2\u0006\u0010\u001d\u001a\u00020\u0006J\u0014\u0010\u001e\u001a\u00020\u001b2\f\u0010\u001f\u001a\b\u0012\u0004\u0012\u00020\t0\bJ\b\u0010 \u001a\u00020\u001bH\u0002J\u000e\u0010!\u001a\b\u0012\u0004\u0012\u00020\t0\bH\u0002J\u000e\u0010\"\u001a\u00020\u001b2\u0006\u0010#\u001a\u00020$J\u0006\u0010%\u001a\u00020\u001bR\u001a\u0010\u0003\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0007\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\t0\b0\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00060\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005X\u0082.\u00a2\u0006\u0002\n\u0000R \u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u000e\"\u0004\b\u000f\u0010\u0010R\u001d\u0010\u0011\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\u00050\u00048F\u00a2\u0006\u0006\u001a\u0004\b\u0012\u0010\u0013R\u001d\u0010\u0014\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\t0\b0\u00048F\u00a2\u0006\u0006\u001a\u0004\b\u0015\u0010\u0013R\u0017\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00060\u00178F\u00a2\u0006\u0006\u001a\u0004\b\u0018\u0010\u0019\u00a8\u0006&"}, d2 = {"Lrashon/com/bbcmvvm/viewmodel/ListViewModel;", "Landroidx/lifecycle/ViewModel;", "()V", "_listOfCharacters", "Landroidx/lifecycle/MutableLiveData;", "Ljava/util/ArrayList;", "Lrashon/com/bbcmvvm/model/BreakingBadCharacter;", "_listOfExistingSeasons", "", "", "_showDetails", "allCharacters", "fakeList", "getFakeList", "()Ljava/util/ArrayList;", "setFakeList", "(Ljava/util/ArrayList;)V", "listOfCharacters", "getListOfCharacters", "()Landroidx/lifecycle/MutableLiveData;", "listOfExistingSeasons", "getListOfExistingSeasons", "navigateToCharacter", "Landroidx/lifecycle/LiveData;", "getNavigateToCharacter", "()Landroidx/lifecycle/LiveData;", "clearFilters", "", "displayCharacter", "breakingBadCharacter", "filterCharactersBySeasons", "selectedSeasons", "getBBCharacters", "getListOfSeasons", "searchCharacters", "searchString", "", "stopDisplayingCharacter", "app_debug"})
public final class ListViewModel extends androidx.lifecycle.ViewModel {
    private java.util.ArrayList<rashon.com.bbcmvvm.model.BreakingBadCharacter> allCharacters;
    private final androidx.lifecycle.MutableLiveData<java.util.ArrayList<rashon.com.bbcmvvm.model.BreakingBadCharacter>> _listOfCharacters = null;
    private final androidx.lifecycle.MutableLiveData<java.util.List<java.lang.Integer>> _listOfExistingSeasons = null;
    @org.jetbrains.annotations.NotNull()
    private java.util.ArrayList<rashon.com.bbcmvvm.model.BreakingBadCharacter> fakeList;
    private final androidx.lifecycle.MutableLiveData<rashon.com.bbcmvvm.model.BreakingBadCharacter> _showDetails = null;
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.ArrayList<rashon.com.bbcmvvm.model.BreakingBadCharacter>> getListOfCharacters() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<java.lang.Integer>> getListOfExistingSeasons() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<rashon.com.bbcmvvm.model.BreakingBadCharacter> getFakeList() {
        return null;
    }
    
    public final void setFakeList(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<rashon.com.bbcmvvm.model.BreakingBadCharacter> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<rashon.com.bbcmvvm.model.BreakingBadCharacter> getNavigateToCharacter() {
        return null;
    }
    
    private final void getBBCharacters() {
    }
    
    public final void filterCharactersBySeasons(@org.jetbrains.annotations.NotNull()
    java.util.List<java.lang.Integer> selectedSeasons) {
    }
    
    public final void searchCharacters(@org.jetbrains.annotations.NotNull()
    java.lang.String searchString) {
    }
    
    private final java.util.List<java.lang.Integer> getListOfSeasons() {
        return null;
    }
    
    public final void clearFilters() {
    }
    
    public final void displayCharacter(@org.jetbrains.annotations.NotNull()
    rashon.com.bbcmvvm.model.BreakingBadCharacter breakingBadCharacter) {
    }
    
    public final void stopDisplayingCharacter() {
    }
    
    public ListViewModel() {
        super();
    }
}