package rashon.com.bbcmvvm.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import rashon.com.bbcmvvm.model.BreakingBadCharacter

class DetailsViewModel(breakingBadCharacter: BreakingBadCharacter, app: Application): AndroidViewModel(app) {
    private val _bBCharacter = MutableLiveData<BreakingBadCharacter>()
    val selectedCharacter: LiveData<BreakingBadCharacter>
        get() = _bBCharacter

    init {
        _bBCharacter.value = breakingBadCharacter
    }

}