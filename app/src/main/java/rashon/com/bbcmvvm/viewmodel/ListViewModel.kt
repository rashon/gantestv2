package rashon.com.bbcmvvm.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import rashon.com.bbcmvvm.model.BreakingBadCharacter
import rashon.com.bbcmvvm.network.BBCApi
import java.util.*

class ListViewModel(): ViewModel() {
    private lateinit var allCharacters: ArrayList<BreakingBadCharacter>
    private val _listOfCharacters = MutableLiveData<ArrayList<BreakingBadCharacter>>()
    private val _listOfExistingSeasons = MutableLiveData<List<Int>>()
    val listOfCharacters: MutableLiveData<ArrayList<BreakingBadCharacter>>
        get() = _listOfCharacters
    val listOfExistingSeasons: MutableLiveData<List<Int>>
        get() = _listOfExistingSeasons
    var fakeList = arrayListOf<BreakingBadCharacter>(BreakingBadCharacter(1, "Name1", "https://www.gstatic.com/devrel-devsite/prod/vdb246b8cc5a5361484bf12c07f2d17c993026d30a19ea3c7ace6f0263f62c0dd/android/images/lockup.svg", arrayListOf(1,2), arrayListOf("Season1", "Season2"), "Nickname1", "unknown"))
    private val _showDetails = MutableLiveData<BreakingBadCharacter>()
    val navigateToCharacter: LiveData<BreakingBadCharacter>
        get() = _showDetails


    init {
        getBBCharacters()
    }
    private fun getBBCharacters(){

        viewModelScope.launch {
            try {
                val listResult = BBCApi.retrofitService.getCharacters()
                _listOfCharacters.value = listResult
                allCharacters = listResult
                _listOfExistingSeasons.value = getListOfSeasons()
            }
            catch (error: Exception){
                Log.d("RRRRR", "That didn't work! $error")
            }
        }
        
    }
    fun filterCharactersBySeasons(selectedSeasons: List<Int>){
        viewModelScope.launch {
            var filteredResult = arrayListOf<BreakingBadCharacter>()
            if (allCharacters != null){
                for (row in allCharacters){
                    for(season in selectedSeasons) {
                        if (row.appearance.contains(season)){
                            filteredResult.add(row)
                            break
                        }
                    }
                }
            }
            _listOfCharacters.value = filteredResult
        }
    }

    fun searchCharacters(searchString: String){
        viewModelScope.launch {
//            val fullList = allCharacters
            var searchResult = arrayListOf<BreakingBadCharacter>()
            if (allCharacters != null) {
                for (row in allCharacters) {
                    if (row.name.toLowerCase(Locale.ROOT).contains(searchString.toLowerCase(Locale.ROOT))) {
                        searchResult.add(row)
                    }
                }
            }
            _listOfCharacters.value = searchResult
        }
    }

    private fun getListOfSeasons(): List<Int>{
        val listOfSeasons = arrayListOf<Int>()
        for (el in _listOfCharacters.value!!){
            for (sel in el.appearance){
                if (!listOfSeasons.contains(sel)){
                    listOfSeasons.add(sel)
                }
            }
        }
        return listOfSeasons
    }

    fun clearFilters(){
        _listOfCharacters.value = allCharacters
    }

    fun displayCharacter(breakingBadCharacter: BreakingBadCharacter){
        _showDetails.value = breakingBadCharacter
    }
    fun stopDisplayingCharacter(){
        _showDetails.value = null
    }

}
//class ListViewModelFactory(): ViewModelProvider.Factory{
//    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
//        if (modelClass.isAssignableFrom(ListViewModel::class.java)){
//            return ListViewModel() as T
//        }
//        throw IllegalArgumentException("Unknown View Model")
//    }
//
//}