package rashon.com.bbcmvvm.model

import android.os.Parcelable
import androidx.versionedparcelable.VersionedParcelize
import kotlinx.parcelize.Parcelize

@Parcelize
data class BreakingBadCharacter(var id: Int, var name: String, var img: String, var appearance: ArrayList<Int>, var occupation: ArrayList<String>, var nickname: String, var status: String): Parcelable{

}