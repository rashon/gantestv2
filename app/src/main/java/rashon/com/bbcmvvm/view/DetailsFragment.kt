package rashon.com.bbcmvvm.view

import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.squareup.picasso.Picasso
import rashon.com.bbcmvvm.R
import rashon.com.bbcmvvm.databinding.FragmentDetailsBinding
import rashon.com.bbcmvvm.databinding.FragmentListBinding
import rashon.com.bbcmvvm.model.BreakingBadCharacter
import rashon.com.bbcmvvm.viewmodel.DetailsViewModel
import rashon.com.bbcmvvm.viewmodel.DetailsViewModelFactory

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"


/**
 * A simple [Fragment] subclass.
 * Use the [DetailsFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class DetailsFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private lateinit var binding: FragmentDetailsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val application = requireNotNull(activity).application
        binding = FragmentDetailsBinding.inflate(inflater)
        binding.lifecycleOwner = this
        val breakingBadCharacter = DetailsFragmentArgs.fromBundle(arguments!!).selectedCharacter
        val viewModelFactory = DetailsViewModelFactory(breakingBadCharacter, application)
        binding.viewModel = ViewModelProvider(this, viewModelFactory).get(DetailsViewModel::class.java)
//        binding = DataBindingUtil.inflate(
//                inflater, R.layout.fragment_details, container, false)
//        // Inflate the layout for this fragment
////        return inflater.inflate(R.layout.fragment_details, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.viewModel!!.selectedCharacter.observe(viewLifecycleOwner, Observer { it ->
            val picasso = Picasso.get()
            picasso.load(it.img).into(binding.charPicture)
            binding.charName.text = it.name
            for (element in it.occupation){
                val textView = TextView(context)
                textView.text = element
                textView.setTextAppearance(android.R.style.TextAppearance_Material_Caption)
                textView.setTextColor(resources.getColor(R.color.black))
                binding.charOccupation.addView(textView)
            }
            binding.charStatus.text = it.status
            for (element in it.appearance){
                val textView = TextView(context)
                textView.text = "Season $element"
                textView.setTextAppearance(android.R.style.TextAppearance_Material_Caption)
                textView.setTextColor(resources.getColor(R.color.black))
                binding.charSeasons.addView(textView)
            }
//            binding.charSeasons.adapter = ArrayAdapter<String>(this.requireContext(), R.layout.custom_list, it.appearance.map { season -> "Season $season" })
        })
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment DetailsFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            DetailsFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}