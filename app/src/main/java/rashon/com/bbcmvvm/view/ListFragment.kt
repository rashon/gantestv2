package rashon.com.bbcmvvm.view

import android.content.DialogInterface
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.SearchView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import rashon.com.bbcmvvm.R
import rashon.com.bbcmvvm.databinding.FragmentListBinding
import rashon.com.bbcmvvm.utils.AdapterBBC
import rashon.com.bbcmvvm.viewmodel.ListViewModel

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"


class ListFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private lateinit var recyclerView: RecyclerView
    private lateinit var binding: FragmentListBinding
    private val listViewModel: ListViewModel by lazy {
        ViewModelProvider(this).get(ListViewModel::class.java)
    }
    private lateinit var checkedSeasons: BooleanArray
    var existingSeasons = listOf<Int>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_list,
            container,
            false
        )
        setHasOptionsMenu(true)
        listViewModel.navigateToCharacter.observe(viewLifecycleOwner, Observer {
            if (it != null){
                this.findNavController().navigate(ListFragmentDirections.actionListFragmentToDetailsFragment(it))
                listViewModel.stopDisplayingCharacter()
            }
        })
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerView = binding.recyclerView
        initializeAdapter()

        listViewModel.listOfExistingSeasons.observe(viewLifecycleOwner, Observer {
            existingSeasons = it
            checkedSeasons = BooleanArray(existingSeasons.size)
        })

//
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.actions_menu, menu)
        val searchView = menu.findItem(R.id.app_bar_search).actionView as SearchView
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(p0: String?): Boolean {
                if (p0 != null) {
                    listViewModel.searchCharacters(p0)
                }
                return false
            }

            override fun onQueryTextChange(p0: String?): Boolean {
                if (p0 != null) {
                    listViewModel.searchCharacters(p0)
                }
                return false
            }

        })

//        val filterButton = menu.findItem(R.id.app_bar_filter).actionView as Button
//        filterButton.setOnClickListener {
//            Toast.makeText(this.requireContext(), "Filter Pressed", Toast.LENGTH_LONG).show()
//
//        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.app_bar_filter) {

            val itemsArray: Array<CharSequence> = existingSeasons.map { it -> "Season $it" }.toTypedArray()


            val builder = AlertDialog.Builder(requireActivity())
            builder.setCancelable(true)
            builder.setNegativeButton("Clear All Filters", DialogInterface.OnClickListener {
                dialog, which ->
                for (i in 0.. checkedSeasons.size - 1){
                    checkedSeasons[i] = false
                }
                listViewModel.clearFilters()
//                (rAdapter as ItemAdapter).filter.filter("87RrSx1FkL")
                item.setIcon(R.drawable.ic_filter_alt_24px)
            })
            builder.setPositiveButton("Apply", DialogInterface.OnClickListener {
                dialog, which ->
                val seasonsToShow: MutableList<Int> = mutableListOf()
                for (i in existingSeasons.indices){
                    if (checkedSeasons[i]){
                        seasonsToShow.add(existingSeasons[i])
                    }
                }
                listViewModel.filterCharactersBySeasons(seasonsToShow)
//                (rAdapter as ItemAdapter).filter.filter("87RrSx1FkL" + seasonsToShow.toString())
                dialog.dismiss()
                if (seasonsToShow.size == 0){
                    item.setIcon(R.drawable.ic_filter_alt_24px)
                }
                else{
                    item.setIcon(R.drawable.ic_filter_accent_alt_24px)
                }

            })
            builder.setMultiChoiceItems(itemsArray, checkedSeasons){dialog, which, isChecked ->
                checkedSeasons[which] = isChecked
            }
            val dialog: AlertDialog = builder.create()
            dialog.setTitle("Filter Characters by Season")
            dialog.show()

        }
        return super.onOptionsItemSelected(item)
    }

    private fun initializeAdapter(){
        recyclerView.layoutManager = LinearLayoutManager(activity)
        observeData()
    }
    fun observeData(){
        listViewModel.listOfCharacters.observe(viewLifecycleOwner, Observer {
//            if (recyclerView.adapter == null) {
                recyclerView.adapter = AdapterBBC(it, { item ->

                    Log.i("RRRRRi", item.toString())
                    listViewModel.displayCharacter(item)
                })
//            }
//            else{
//                recyclerView.adapter!!.notifyDataSetChanged()
//            }
        })
    }


    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment ListFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            ListFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}